package jp.co.lerning.mapper;

import java.util.List;

import jp.co.lerning.entity.ExplanationEntity;

public interface ExplanationMapper {
	//全件取得
	List<ExplanationEntity> getExplanation();
}
