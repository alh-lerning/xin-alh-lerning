package jp.co.lerning.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.lerning.dto.SelectDto;
import jp.co.lerning.entity.SelectEntity;
import jp.co.lerning.mapper.SelectMapper;

@Service
public class SelectService {
	@Autowired
	private SelectMapper selectMapper;

	//選択肢の全件取得
	public List<SelectDto> getSelectAll() {
		List<SelectEntity> selectList = selectMapper.getSelectAll();
		List<SelectDto> resultList = converToDto(selectList);
		return resultList;
	}

	private List<SelectDto> converToDto(List<SelectEntity> selectList) {
		List<SelectDto> resultList = new LinkedList<SelectDto>();
		for (SelectEntity entity : selectList) {
			SelectDto dto = new SelectDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	//採点結果表示
	public int getResult() {
		List<SelectEntity> selectList = selectMapper.getSelectAll();
		int SresultList = convertToDto(selectList);
		return SresultList;
	}

	private int convertToDto(List<SelectEntity> selectList) {
		List<SelectDto> SresultList = new LinkedList<SelectDto>();
		for (SelectEntity entity : selectList) {
			SelectDto dto = new SelectDto();
			BeanUtils.copyProperties(entity, dto);
			SresultList.add(dto);
		}
		return SresultList.size();
	}

	//ユーザーが選んだ選択肢の挿入
	public int selectInsert(int choiceId, int questionId) {
		int count = selectMapper.insertSelect(choiceId, questionId);
		return count;
	}
}
