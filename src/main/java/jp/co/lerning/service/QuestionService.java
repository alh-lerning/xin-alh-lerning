package jp.co.lerning.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.lerning.dto.QuestionDto;
import jp.co.lerning.entity.QuestionEntity;
import jp.co.lerning.mapper.QuestionMapper;

@Service
public class QuestionService {
	@Autowired
	private QuestionMapper questionMapper;

	//問題の全件取得
	public List<QuestionDto> getQuestionAll() {
		List<QuestionEntity> questionList = questionMapper.getQuestionAll();
		List<QuestionDto> resultList = convertToDto(questionList);
		return resultList;
	}

	private List<QuestionDto> convertToDto(List<QuestionEntity> questionList) {
		List<QuestionDto> resultList = new LinkedList<QuestionDto>();
		for (QuestionEntity entity : questionList) {
			QuestionDto dto = new QuestionDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}
}
