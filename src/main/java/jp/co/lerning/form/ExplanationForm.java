package jp.co.lerning.form;

public class ExplanationForm {
	private String text;
    private Integer questionId;

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
}
