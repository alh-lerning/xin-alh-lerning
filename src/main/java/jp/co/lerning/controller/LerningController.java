package jp.co.lerning.controller;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.lerning.dto.ExplanationDto;
import jp.co.lerning.dto.LerningDto;
import jp.co.lerning.dto.QuestionDto;
import jp.co.lerning.dto.SelectDto;
import jp.co.lerning.form.SelectForm;
import jp.co.lerning.service.ExplanationService;
import jp.co.lerning.service.LerningService;
import jp.co.lerning.service.QuestionService;
import jp.co.lerning.service.SelectService;

@Controller
public class LerningController {

	//プルしてコミットしてプッシュ
	@Autowired
	private LerningService lerningService;
	@Autowired
	private SelectService selectService;
	@Autowired
	private QuestionService questionService;
	@Autowired
	private ExplanationService explanationService;

	//問題の全件取得
	@RequestMapping (value = "/question", method = RequestMethod.GET)
	public String messageAll(Model model) {
		List<QuestionDto> questions = questionService.getQuestionAll();
		List<SelectDto> selects = selectService.getSelectAll();
		List<LerningDto> answers = lerningService.getAllId();
		model.addAttribute("questions", questions);
		model.addAttribute("selects", selects);
		model.addAttribute("answers", answers);
		return "showMessage";
	}

	//解答表示用	//解説の全件取得
	@RequestMapping(value = "/result", method = RequestMethod.GET)
	public String textAll(Model model) {
		List<LerningDto> ids = lerningService.getAllId();
		model.addAttribute("ids", ids);

		List<SelectDto> selects = selectService.getSelectAll();
		model.addAttribute("select", selects);

		int result = selectService.getResult();
		model.addAttribute("result", result);

		List<QuestionDto> questions = questionService.getQuestionAll();
		model.addAttribute("questions", questions);

		List<SelectDto> select = selectService.getSelectAll();
		model.addAttribute("select", select);

		List<ExplanationDto> texts = explanationService.getExplanation();
		model.addAttribute("texts", texts);

		model.addAttribute("good", "正解");
		model.addAttribute("bad", "不正解");
		model.addAttribute("sentence", "解説");

		return "answer";
	}

	//ユーザーが選択した選択肢のidを挿入
	@RequestMapping(value = "/result", method = RequestMethod.POST)
	public String selectInsert(@ModelAttribute SelectForm form, Model model) {
		int count = selectService.selectInsert(form.getChoiceId(), form.getQuestionId());
		Logger.getLogger(LerningController.class.toString()).log(Level.INFO, "挿入件数は" + count + "件です。");

		return "redirect:/result";
	}
}
