<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
	<head>
		<meta charset="utf-8">
		<title>問題画面</title>
	</head>

	<body>
		<div class="questions">
			<form:form modelAttribute="selectForm" action="/e-lerning/result">
				<c:forEach items="${questions}" var="question">
					<div class="question">
						<span class="text"><c:out value="${question.id}" /></span>
						<c:out value="${question.text}" /><br />
					</div>
					<div class="selects">
						<c:forEach items="${selects}" var="select">
							<c:if test="${question.id == select.questionId }">
								<input name="${select.questionId}" type="radio" value="${select.choiceId}" /><c:out value="${select.text}" /><br/>
							</c:if>
						</c:forEach>
					</div>
				</c:forEach>
				<br>
				<input type="submit" value="回答">
			</form:form>
		</div>
		<br />
		<br />
		<br />
	</body>
</html>